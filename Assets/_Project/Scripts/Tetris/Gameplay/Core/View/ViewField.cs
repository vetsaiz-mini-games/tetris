﻿using UnityEngine;
using UnityEngine.UI;

namespace Tetris.Gameplay.Core.View
{
    public class ViewField : MonoBehaviour 
    {
        
        [SerializeField]
        private GridLayoutGroup grid;

        [SerializeField]
        private Image _cellPrefab;

        private Image[] _images;
        public void Init(Settings settings)
        {
            if (_images != null)
            {
                foreach (var temp in _images)
                {
                    Destroy(temp.gameObject);
                }
            }
            
            grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            grid.constraintCount = settings.FieldSize.x;
            _images = new Image[settings.FieldSize.x * settings.FieldSize.y];
            for (var i = 0; i < settings.FieldSize.x * settings.FieldSize.y; i++) {
                _images[i] = Object.Instantiate(_cellPrefab, grid.transform);
            }
        }
        public void FieldOnUpdated(bool[] data) 
        {
            for (var i = 0; i < data.Length; i++) 
            {
                _images[i].color = data[i] ? Color.black : Color.white;
            }
        }
    }
}
