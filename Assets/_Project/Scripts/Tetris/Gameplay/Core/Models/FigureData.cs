﻿using System;
using UnityEngine;

namespace Tetris.Gameplay.Core.Models 
{
    [Serializable]
    public class FigureData {
        public Vector2Int Size;

        public bool[] Data;

        public bool this[int x, int y] {
            get { return Data[x + y * Size.x]; }
            set { Data[x + y * Size.x] = value; }
        }

        public FigureData() {
        }

        public FigureData(int x, int y) {
            Size = new Vector2Int(x, y);
            Clear();
        }

        public FigureData Copy()
        {
            var data = new bool[Data.Length];
            for (var i = 0; i < data.Length; i++) {
                data[i] = Data[i];
            }

            return new FigureData() {
                Size = Size,
                Data = data
            };
        }

        public void Clear()
        {
            Data = new bool[Size.x * Size.y];
        }
    }


    public class GameFigureWrap
    {
        public FigureData Figure;
        public Vector2Int Pos;
    }
}