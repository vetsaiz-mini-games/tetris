﻿using System;
using UnityEngine;

namespace Tetris.Gameplay.Core.Models
{
    public class Field: IDisposable
    {
        public readonly FigureData FieldData;

        public Field(Vector2Int size)
        {
            FieldData = new FigureData(size.x, size.y);
        }

        public bool[] Update(GameFigureWrap figure)
        {
            bool[] data = new bool[FieldData.Size.x * FieldData.Size.y];
            Array.Copy(FieldData.Data, data, FieldData.Data.Length);
            ApplyFigureData(data, figure);
            return data;
        }

        public void ApplyFigureData(bool[] data, GameFigureWrap figure)
        {
            for (var x = 0; x < figure.Figure.Size.x; x++) {
                for (var y = 0; y < figure.Figure.Size.y; y++) {
                    var px = figure.Pos.x + x;
                    var py = figure.Pos.y + y;
                    data[px + FieldData.Size.x * py] = data[px + FieldData.Size.x * py] || figure.Figure[x, y];
                }
            }
        }

        public bool TrySpawnFigure(GameFigureWrap spawn)
        {
            for (var x = spawn.Pos.x; x < spawn.Pos.x + spawn.Figure.Size.x; x++) {
                for (var y = spawn.Pos.y; y < spawn.Pos.y + spawn.Figure.Size.y; y++) {
                    if (FieldData[x, y]) {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool TestFigure(FigureData figure, Vector2Int pos)
        {
            if (pos.x + figure.Size.x > FieldData.Size.x)
                return false;
            if (pos.x < 0)
                return false;
            if (pos.y + figure.Size.y > FieldData.Size.y)
                return false;
            for (var x = pos.x; x < figure.Size.x + pos.x; x++) {
                for (var y = pos.y; y < figure.Size.y + pos.y; y++) {
                    if (FieldData[x, y] && figure[x - pos.x, y - pos.y])
                        return false;
                }
            }
            return true;
        }

        public bool IsGameOver()
        {
            for (var x = 0; x < FieldData.Size.x; x++)
            {
                if (FieldData[x, 0])
                {
                    return true;
                }
            }
            return false;
        }

        public void TestAndRemove()
        {
            var y = FieldData.Size.y - 1;
            while (y > 0) {
                var isFilled = true;
                for (var x = 0; x < FieldData.Size.x; x++) {
                    isFilled = isFilled && FieldData[x, y];
                }
                if (isFilled) {
                    for (var y2 = y; y2 > 0; y2--) {
                        for (var x = 0; x < FieldData.Size.x; x++) {
                            FieldData[x, y2] = FieldData[x, y2 - 1];
                        }
                    }
                } else {
                    y--;
                }
            }
        }

        public void Dispose()
        {
            FieldData.Clear();
        }
    }
}