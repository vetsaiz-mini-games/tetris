﻿namespace Tetris.Gameplay.Core
{
    public enum UpdateState
    {
        Left,
        Right,
        Down,

        Continue,
    }
}
