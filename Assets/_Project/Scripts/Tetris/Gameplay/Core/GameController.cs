﻿using System;
using System.Collections.Generic;
using Tetris.Gameplay.Core.Models;
using Tetris.Gameplay.Core.View;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tetris.Gameplay.Core
{
    public class GameController : IDisposable
    {
        private readonly Field _field;
        private GameFigureWrap _currentFigure;
        private readonly List<FigureData> _figureSamples;
        private readonly ViewField _view;
        private readonly Settings _settings;
        private GameResult _result;
        private readonly UpdateController _update;

        public event Action<bool[]> OnDataUpdate;

        public GameController(Settings settings, ViewField view, List<FigureData> figures)
        {
            _settings = settings;

            _field = new Field(_settings.FieldSize);
            _view = view;
            _figureSamples = figures;

            _update = UpdateController.Create(InputUpdate, settings);

            GameStateManager.Instance.OnChangeState += ChangeState;
        }

        private void ChangeState(GameMode mode)
        {
            if (mode == GameMode.Game)
            {
                _result = new GameResult();
                _view.Init(_settings);
                OnDataUpdate += _view.FieldOnUpdated;
                _update.OnChangeLevel += ChangeLevel; 
                StartNewGame();
            }
            else
            {
                _update.OnChangeLevel -= ChangeLevel;
                OnDataUpdate -= _view.FieldOnUpdated;
            }
        }

        private void ChangeLevel()
        {
            _result.Scorer++;
        }

        private void InputUpdate(UpdateState state)
        {
            switch (state) {
                case UpdateState.Left:
                    MoveLeft();
                    break;
                case UpdateState.Right:
                    MoveRight();
                    break;
                case UpdateState.Down:
                    MoveDown();
                    break;
                case UpdateState.Continue:
                    StartNewGame();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private void StartNewGame()
        {
            _result = new GameResult();
            _field.Dispose();
            SpawnFigure();
        }

        private void MoveDown()
        {
            if (_field.TestFigure(_currentFigure.Figure, _currentFigure.Pos + Vector2Int.up)) {
                _currentFigure.Pos.y++;
                UpdateField();
            }
            else {
                _field.ApplyFigureData(_field.FieldData.Data, _currentFigure);
                _field.TestAndRemove();
                if (_field.IsGameOver()) {
                    SetGameOver();
                }
                else {
                    SpawnFigure();
                    UpdateField();
                }
            }
        }

        private void SetGameOver()
        {
            GameStateManager.Instance.SetMenu(_result);
        }

        private void MoveRight()
        {
            if (_field.TestFigure(_currentFigure.Figure, _currentFigure.Pos + Vector2Int.right))
            {
                _currentFigure.Pos.x = _currentFigure.Pos.x + 1;
                UpdateField();
            }
        }

        private void MoveLeft()
        {
            if (_field.TestFigure(_currentFigure.Figure, _currentFigure.Pos + Vector2Int.left))
            {
                _currentFigure.Pos.x = _currentFigure.Pos.x - 1;
                UpdateField();
            }
        }

        private void SpawnFigure()
        {
            var figure = _figureSamples[Random.Range(0, int.MaxValue) % _figureSamples.Count].Copy();
            _currentFigure = new GameFigureWrap()
            {
                Figure = figure,
                Pos = new Vector2Int(_field.FieldData.Size.x / 2 - figure.Size.x / 2, 0)
            };
            UpdateField();
        }

        private void UpdateField()
        {
            var data = _field.Update(_currentFigure);
            OnDataUpdate?.Invoke(data);
        }

        public void Dispose()
        {
            GameStateManager.Instance.OnChangeState -= ChangeState;
        }
    }
}
