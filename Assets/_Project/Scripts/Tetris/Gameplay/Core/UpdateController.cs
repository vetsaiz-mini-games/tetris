﻿using System;
using UnityEngine;

namespace Tetris.Gameplay.Core
{
    class UpdateController : MonoBehaviour
    {
        private float _forceFactor;
        private float _timeUpdate;
        private float _forceUpdate;
        private float _forceVelocityFactor;
        private float _startVelocity;

        private float _currentVelocity;
        private float _currentTime;
        private float _forceTime;
        private static Action<UpdateState> _callback;
        private GameMode _state;
        public event Action OnChangeLevel;
        public static UpdateController Create(Action<UpdateState> callback, Settings settings)
        {
            var gm = new GameObject("Updater");
            var inst = gm.AddComponent<UpdateController>();
            inst._forceFactor = settings.ForceFactor;
            inst._timeUpdate = settings.TimeUpdate;
            inst._forceUpdate = settings.ForceUpdate;
            inst._forceVelocityFactor = settings.ForceVelocityFactor;
            inst._startVelocity = settings.StartVelocity;
            GameStateManager.Instance.OnChangeState += inst.SetState;
            _callback = callback;
            return inst;
        }

        public void SetState(GameMode mode)
        {
            _currentVelocity = _startVelocity;
            _currentTime = _timeUpdate;
            _forceTime = _forceUpdate;
            _state = mode;
        }
        
        void Update()
        {
            if (_state == GameMode.Pause) {
                if (Input.GetKeyDown(KeyCode.Space))
                    _callback(UpdateState.Continue);
                return;
            }
            UpdateSpeed();
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                _callback(UpdateState.Left);
            if (Input.GetKeyDown(KeyCode.RightArrow))
                _callback(UpdateState.Right);

            UpdateTime(Input.GetKey(KeyCode.DownArrow));
        }

        private void UpdateSpeed()
        {
            _forceTime -= Time.deltaTime;
            if (_forceTime < 0) {
                _currentVelocity += _forceVelocityFactor;
                OnChangeLevel?.Invoke();
            }
        }

        private void UpdateTime(bool isForce)
        {
            _currentTime -= Time.deltaTime * (_currentVelocity * (isForce ? _forceFactor : 1));
            if (_currentTime < 0) {
                _currentTime = _timeUpdate;
                _callback(UpdateState.Down);
            }
        }
    }
}