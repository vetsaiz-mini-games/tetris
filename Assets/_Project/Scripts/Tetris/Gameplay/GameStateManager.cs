﻿using System;
using Tetris.Gameplay.Meta.Gui.ViewModels;
using Tetris.Gameplay.Meta.Logic;
using Tetris.Gui;
using Tetris.Services;

namespace Tetris.Gameplay
{
    public class GameStateManager //TODO Сделать какой то более удобоваримый DI
    {
        public static GameStateManager Instance => _instance;

        private static GameStateManager _instance;
        private IGuiService _gui;
        public ILogicModels Models => _models;

        public event Action<GameMode> OnChangeState;

        private LogicModels _models;

        private GameStateManager(GuiService service, LogicModels logic)
        {
            _gui = service;
            _models = logic;
        }
    
        public void SetMenu(GameResult result)
        {
            _models.ScorerModel.SetScorer(result.Scorer);
            _gui.Show<MenuViewModel>();
            OnChangeState?.Invoke(GameMode.Pause);
        }

        public void SetGame()
        {
            _gui.Hide<MenuViewModel>();
            OnChangeState?.Invoke(GameMode.Game);
        }

        public static void Create(GuiService service, LogicModels logic)
        {
            _instance = new GameStateManager(service, logic);
        }
    }
}
