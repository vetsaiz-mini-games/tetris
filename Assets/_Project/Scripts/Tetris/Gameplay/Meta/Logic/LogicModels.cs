﻿namespace Tetris.Gameplay.Meta.Logic
{
    public class LogicModels : ILogicModels
    {
        public IScorerModel Scorer => ScorerModel;

        public ScorerModel ScorerModel;
        public LogicModels()
        {
            ScorerModel = new ScorerModel();
        }
    }
}
