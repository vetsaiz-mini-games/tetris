﻿namespace Tetris.Gameplay.Meta.Logic
{
    public class ScorerModel : IScorerModel
    {
        public int Current { get; private set; }

        public void SetScorer(int scorer)
        {
            Current = scorer;
        }
    }
}
