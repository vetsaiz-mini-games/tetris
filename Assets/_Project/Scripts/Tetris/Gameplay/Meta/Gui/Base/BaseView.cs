﻿using Tetris.Gui;
using UnityEngine;

namespace Tetris.Gameplay.Meta.Gui
{
    public class BaseView : MonoBehaviour, IView
    {
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
