﻿using Tetris.Gui;

namespace Tetris.Gameplay.Meta.Gui.ViewModels
{
    public class MenuViewModel : IViewModel
    {
        public MenuViewModel()
        {

        }
        public int Scorer => GameStateManager.Instance.Models.Scorer.Current;

        public void StartGame()
        {
            GameStateManager.Instance.SetGame();
        }
    }
}
