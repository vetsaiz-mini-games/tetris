﻿using Tetris.Gameplay.Meta.Gui.ViewModels;
using Tetris.Gui;
using UnityEngine;
using UnityEngine.UI;

namespace Tetris.Gameplay.Meta.Gui.Views
{
    public class MenuView : BaseView, IView<MenuViewModel>
    {
        [SerializeField]
        private Text _scorer;

        private MenuViewModel _model;
        public void Bind(MenuViewModel model)
        {
            _model = model;
        }

        public override void Show()
        {
            base.Show();
            _scorer.text = $"Предыдущие очки: {_model.Scorer}";
        }
    
        public void StartGame()
        {
            _model.StartGame();
        }
    }
}
