﻿namespace Tetris.Gameplay.Meta.Logic
{
    public interface ILogicModels
    {
        IScorerModel Scorer { get; }
    }

    public interface IScorerModel 
    { 
        int Current { get; }
    }
}