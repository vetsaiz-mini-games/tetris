﻿using UnityEngine;

namespace Tetris
{
    [CreateAssetMenu(fileName = "settings", menuName = "Create/Settings", order = 1)]
    public class Settings : ScriptableObject
    {
        public Vector2Int FieldSize;
    
    
    
    

        public float ForceFactor;
        public float TimeUpdate;
        public float ForceUpdate;
        public float ForceVelocityFactor;
        public float StartVelocity;
    }
}