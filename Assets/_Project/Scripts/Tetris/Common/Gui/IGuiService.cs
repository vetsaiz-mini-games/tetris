﻿namespace Tetris.Gui
{
    public interface IGuiService
    {
        void Show<T>() where T : IViewModel;
        void Hide<T>() where T : IViewModel;
    }
}
