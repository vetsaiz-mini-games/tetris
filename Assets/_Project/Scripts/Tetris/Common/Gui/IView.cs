﻿namespace Tetris.Gui
{
    public interface IView 
    {
        void Show();
        void Hide();
    }


    public interface IView<T> : IView
    {
        void Bind(T model);
    }
}