﻿using System;
using System.Collections.Generic;
using Tetris.Gui;
using UnityEngine;

namespace Tetris.Services
{
    public class GuiService : MonoBehaviour, IGuiService
    {
        private readonly Dictionary<Type, IView> _map = new Dictionary<Type, IView>();

        private readonly GameObject _container;
    
        public static GuiService Create(GameObject container)
        {
            return new GuiService(container);
        }

        private GuiService(GameObject container)
        {
            _container = container;
        }
    
        public void Register<TModel, TView>(TView view)
            where TModel : IViewModel, new()
            where TView : MonoBehaviour, IView<TModel>
        {
            _map[typeof(TModel)] = view;
            view.transform.SetParent(_container.transform, false);
            view.Bind(new TModel());
        }

        public void Show<T>() where T : IViewModel
        {
            if (_map.TryGetValue(typeof(T), out var view))
            {
                view.Show();
            }
        }

        public void Hide<T>() where T : IViewModel
        {
            if (_map.TryGetValue(typeof(T), out var view))
            {
                view.Hide();
            }
        }
    }
}
