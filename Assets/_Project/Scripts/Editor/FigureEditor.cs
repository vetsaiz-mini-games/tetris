﻿using System.IO;
using Tetris.Gameplay.Core.Models;
using UnityEditor;
using UnityEngine;

namespace Editor {

    public class FigureEditor : EditorWindow {
        private FigureDataWrap _current; 
        private const string FIGURE_PATH = "Assets/_Project/Resources/Figures";
    
        [MenuItem("Tetris/FigureEditor")]
        public static void ShowFigureEditor() {
            CreateInstance<FigureEditor>().Show();
        }

        private class FigureDataWrap {
            public FigureData Data;
            public string Name;
        }
    
        private void OnGUI() {
            LoadGui();
            NewGui();
            EditGui();
            SaveGui();
        }

        private void NewGui() {
            if (GUILayout.Button("New figure")) {
                _current = new FigureDataWrap() {
                    Name = "NewFigure",
                    Data = new FigureData() {
                        Size = new Vector2Int(3, 3),
                        Data = new bool[9]
                    }
                };
            }
        }

        private void LoadGui() {
            if (GUILayout.Button("Load")) {
                var path = EditorUtility.OpenFilePanel("", FIGURE_PATH, "json");
                if (string.IsNullOrEmpty(path)) {
                    return;
                }
                var data = JsonUtility.FromJson<FigureData>(File.ReadAllText(path));
                if (data == null)
                    return;

                _current = new FigureDataWrap() {
                    Data = data,
                    Name = Path.GetFileNameWithoutExtension(path)
                };
            }
        }

        private void SaveGui() {
            if (_current != null) {
                if (GUILayout.Button("Save")) {
                    File.WriteAllText(Path.Combine(FIGURE_PATH, _current.Name + ".json"), JsonUtility.ToJson(_current.Data));
                    AssetDatabase.Refresh();
                }
            }
        }

        private void EditGui() {
            if (_current != null) {
                _current.Name = EditorGUILayout.TextField("name:", _current.Name);
                var newSizeX = EditorGUILayout.IntField("width:", _current.Data.Size.x);
                var newSizeY = EditorGUILayout.IntField("height:",_current.Data.Size.y);
                if (newSizeX != _current.Data.Size.x || newSizeY != _current.Data.Size.y) {
                    _current.Data.Data = new bool[newSizeX * newSizeY];
                    _current.Data.Size = new Vector2Int(newSizeX, newSizeY);
                }
                GUILayout.BeginVertical("Box");
                for (int y = 0; y < _current.Data.Size.y; y++) {
                    GUILayout.BeginHorizontal();
                    for (int x = 0; x < _current.Data.Size.x; x++) {
                        if (GUILayout.Button(_current.Data[x, y] ? "x" :" ")) {
                            _current.Data[x, y] = !_current.Data[x, y];
                        }
                    }
                    GUILayout.EndHorizontal();
                }
                GUILayout.EndVertical();
            }
        }
    }
}