﻿using System.Linq;
using Tetris;
using Tetris.Gameplay;
using Tetris.Gameplay.Core;
using Tetris.Gameplay.Core.Models;
using Tetris.Gameplay.Core.View;
using Tetris.Gameplay.Meta.Gui.ViewModels;
using Tetris.Gameplay.Meta.Gui.Views;
using Tetris.Gameplay.Meta.Logic;
using Tetris.Services;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    [RuntimeInitializeOnLoadMethod]
    private static void Main() {

        var settings = Resources.Load<Settings>("settings");
        var view = Object.Instantiate(Resources.Load<ViewField>("GameView"));
        var root = Object.Instantiate(Resources.Load<GameObject>("GuiRoot"));
        var figureSamples = Resources.LoadAll<TextAsset>("Figures")
            .Select(e => JsonUtility.FromJson<FigureData>(e.text))
            .ToList();


        var logic = new LogicModels();
        var service = GuiService.Create(root);
        service.Register<MenuViewModel, MenuView>(Object.Instantiate(Resources.Load<MenuView>("Gui/MenuView")));

        GameStateManager.Create(service, logic);
        
        var game = new GameController(settings, view, figureSamples);
        GameStateManager.Instance.SetMenu(new GameResult());
    }
}
